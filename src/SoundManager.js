/**
 * Created by Grzegorz on 23.03.14.
 */
import AudioManager;
var SoundManager = exports = Class(AudioManager, function (supr) {
    var defaults = {
        path: "resources/sounds/mainMenu",
        files: {
            background: {
                volume: 0.6,
                loop: true,
                background: true
            }
        }
    };
    this.init = function (opts) {
        this._opts = opts = merge(opts, defaults);
        supr(this, 'init', arguments);
    }
});
SoundManager.prototype.playInBacground= function(fileName){
    this.play(fileName, {loop: true});
}
