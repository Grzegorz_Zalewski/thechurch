import ui.TextView as TextView;
import ui.SpriteView;
import ui.ImageView;
import ui.View as View;
import ui.ViewPool as ViewPool;
import ui.ImageScaleView;
import ui.resource.loader as loader;
import menus.views.MenuView as MenuView;
import AudioManager;
//import src.SoundManager as SoundManager;

import src.ParallaxViews as ParallaxViews;

exports = Class(GC.Application, function () {
	this.initUI = function () {
        window.lang= JSON.parse(CACHE['resources/lang/pl.json']);
        this.mainMenu = new MenuView({
            superview: this,
            title: lang.theChurch,
            items: [
                {item: lang.play, action: bind(this, 'onOption')},
                {item: lang.options , action: 'Second'}
            ]
        });
        this.createBG = function (){
            //instantiate the obj
            this.parallaxViews = new ParallaxViews({
                superview: this.view,
                width: this.view.style.width,
                height: this.view.style.height
            });
            this.parallaxViews.addBGView(new ui.ImageScaleView({
                scaleMethod: 'cover',
                image: 'resources/images/level/backgroundSky.png'
            }));
            //create a parallax view pool of images to scroll
            //this.createViewLayer = function(layer, link, x, y, speed, w, h, offset)
            this.parallaxViews.createViewLayer(0, 'resources/images/level/farground-church.png', 0, 10,1000*100, 512,512, 0);
            this.parallaxViews.createViewLayer(1, 'resources/images/level/fargroundBrush.png', 0, 190, 900*15, 512, 106, 0);
            this.parallaxViews.createViewLayer(2, 'resources/images/level/midgroundBrush.png', 0, 200, 500*15, 512, 106, 0);
            this.parallaxViews.createViewLayer(3, 'resources/images/level/cloud5.png', 0, 10, 1000*15, 380, 53, 0);
            this.parallaxViews.createViewLayer(4, 'resources/images/level/cloud4.png', 10, 20, 950*15, 380, 53, 0);
            this.parallaxViews.createViewLayer(5, 'resources/images/level/cloud3.png', 15, 25, 1200*15, 380, 53, 0);
            this.parallaxViews.createViewLayer(6, 'resources/images/level/cloud2.png', 40, 50, 1800*15, 380, 53, 0);
            this.parallaxViews.createViewLayer(7, 'resources/images/level/cloud1.png', 70, 80, 700*15, 380, 53, 0);

        }
        this._sound = new AudioManager({
            path: "resources/sounds/mainMenu",
            files: {
                background: {
                    volume: 0.6,
                    loop: true,
                    background: true
                }
            }
        });
        //this._sound = new SoundManager();
    };

	this.launchUI = function () {
    var   context = this;
        window.con=this;
    loader.preload('resources/', function () {
        context.createBG();
        context.mainMenu.show();
        context._sound.play("background", {loop: true});
        //context._sound.playInBacground("background");
        });
    };
});
